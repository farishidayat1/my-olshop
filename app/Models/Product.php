<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'products';

    protected $fillable = [
        'name',
        'price',
        'category_id',
        'image_url',
    ];

    protected $appends = [
        'signed_image_url',
        'price_formatted'
    ];

    public function getSignedImageUrlAttribute() 
    {
        return asset('myfiles/'.$this->image_url);
    }

    public function category()
    {
        return $this->belongsTo(ProductCategory::class, 'category_id');
    }

    public function getPriceFormattedAttribute() 
    {
        return number_format($this->price, 0);
    }

}
