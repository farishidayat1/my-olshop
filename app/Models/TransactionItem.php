<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransactionItem extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'transaction_items';

    protected $fillable = [
        'transaction_id',
        'product_id',
        'quantity',
        'total_price'
    ];

    protected $appends = [
        'total_price_formatted'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function getTotalPriceFormattedAttribute() 
    {
        return number_format($this->total_price, 0);
    }
}
