<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'transactions';

    protected $fillable = [
        'name',
        'phone',
        'email',
        'address',
        'address_two',
        'proof_of_payment',
    ];

    protected $appends = [
        'signed_proof_of_payment_url',
    ];

    public function items()
    {
        return $this->hasMany(TransactionItem::class, 'transaction_id', 'id');
    }

    public function getSignedProofOfPaymentUrlAttribute() 
    {
        return asset('myfiles/'.$this->proof_of_payment);
    }
}
