<?php

namespace App\Http\Controllers\Application\Admin;

use App\Http\Controllers\Controller;
use App\Models\ProductCategory;
use Illuminate\Http\Request;

class ProductCategoryController extends Controller
{
    public function index()
    {
        $productCategories = ProductCategory::orderBy('created_at', 'desc')->get();

        return view('app.admin.product-category.index', [
            'product_categories' => $productCategories
        ]);
    }

    public function create() 
    {
        return view('app.admin.product-category.create');
    }


    public function edit($id) 
    {
        $product_category = ProductCategory::where('id', $id)->first();

        return view('app.admin.product-category.edit', [
            'product_category' => $product_category
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $product = new ProductCategory();
        $product->name = $request->name;
        $product->save();
        
        return redirect()->route('products-category')->with('success_message', 'Successfully created category!');
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'name' => 'required',
        ]);

        $product = ProductCategory::where('id', $request->id)->first();
        $product->name = $request->name;
        $product->save();
        
        return redirect()->route('products-category')->with('success_message', 'Successfully updated category!');
    }

    public function destroy(Request $request) 
    {
        ProductCategory::where('id', $request->id)->delete();

        return redirect()->route('products-category')->with('success_message', 'Successfully deleted category!');
    }
}
