<?php

namespace App\Http\Controllers\Application\Admin;

use App\Http\Controllers\Controller;
use App\Models\Transaction;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    public function index()
    {
        $transactions = Transaction::orderBy('created_at', 'desc')->get();

        return view('app.admin.transaction.index', [
            'transactions' => $transactions
        ]);
    }

    public function show($id)
    {
        $transaction = Transaction::where('id', $id)->with(['items', 'items.product'])->first();

        return view('app.admin.transaction.show', [
            'transaction' => $transaction
        ]);
    }
}
