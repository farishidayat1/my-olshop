<?php

namespace App\Http\Controllers\Application\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view('app.admin.dashboard');
    }
}
