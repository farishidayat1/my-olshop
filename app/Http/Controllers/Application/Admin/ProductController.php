<?php

namespace App\Http\Controllers\Application\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductCategory;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::orderBy('created_at', 'desc')->get();

        return view('app.admin.product.index', [
            'products' => $products
        ]);
    }

    public function create() 
    {
        $productCategories = ProductCategory::orderBy('created_at', 'desc')->get();

        return view('app.admin.product.create', [
            'product_categories' => $productCategories
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'price' => 'required',
            'category_id' => 'required',
            'image' => 'required'
        ]);

        $product = new Product();
        $product->name = $request->name;
        $product->price = $request->price;
        $product->category_id = $request->category_id;
        $product->image_url = $request->file('image')->store('product-images', ['disk' => 'my_files']);
        $product->save();
        
        return redirect()->route('products')->with('success_message', 'Successfully created products!');
    }

    public function edit($id) 
    {
        $product = Product::where('id', $id)->first();
        $productCategories = ProductCategory::orderBy('created_at', 'desc')->get();

        return view('app.admin.product.edit', [
            'product' => $product,
            'product_categories' => $productCategories
        ]);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'name' => 'required',
            'price' => 'required',
            'category_id' => 'required',
        ]);

        $product = Product::where('id', $request->id)->first();
        $product->name = $request->name;
        $product->price = $request->price;
        $product->category_id = $request->category_id;
        if($request->file('image')) {
            $product->image_url = $request->file('image')->store('product-images', ['disk' => 'my_files']);
        }
        $product->save();
        
        return redirect()->route('products')->with('success_message', 'Successfully updated product!');
    }

    public function destroy(Request $request) 
    {
        Product::where('id', $request->id)->delete();

        return redirect()->route('products')->with('success_message', 'Successfully deleted product!');
    }
}
