<?php

namespace App\Http\Controllers\Application\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\Transaction;
use App\Models\TransactionItem;
use Facade\FlareClient\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class HomeController extends Controller
{
    public function index()
    {
        $products = Product::orderBy('created_at', 'desc')->get();

        return view('app.frontend.index', [
            'products' => $products
        ]);
    }

    public function cart()
    {
        $product_carts = Cookie::get('product_carts');

        $productCarts = ! empty($product_carts) ? json_decode($product_carts) : [];

        return view('app.frontend.cart', [
            'productCarts' => $productCarts,
            'countProduct' => count($productCarts)
        ]);
    }

    public function shop(Request $request)
    {
        $tab = $request->tab ?? 'all';
        $products = Product::orderBy('created_at', 'desc');
        if(! empty($tab) && $tab != 'all') {
            $productCategoryId = ProductCategory::where('name', $tab)->first()->id;
            $products = $products->where('category_id', $productCategoryId);

        }
        $products = $products->get();
        $productCategories = ProductCategory::orderBy('created_at', 'desc')->get();

        return view('app.frontend.shop', [
            'products' => $products,
            'productCategories' => $productCategories,
            'tab' => $tab
        ]);
    }

    public function addCart(Request $request) 
    {
        $product_carts = Cookie::get('product_carts');

        $productCarts = ! empty($product_carts) ? json_decode($product_carts) : [];

        $product = Product::select('id', 'name', 'price')->where('id', $request->product_id)->first();
        $product->quantity = $request->quantity;
        $total_price = $product->price * $product->quantity;
        $product->total_price = $total_price;
        $product->total_price_formatted = number_format($total_price, 0);

        $productCarts[] = $product;

        Cookie::queue('product_carts', json_encode($productCarts), 120);

        return redirect()->route('shop-page')->with('success_message', 'Successfully add to cart');
    }

    public function checkout(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'address' => 'required',
            'address_two' => 'required',
            'proof_of_payment' => 'required'
        ]);

        $transaction = new Transaction();
        $transaction->name = $request->name;
        $transaction->phone = $request->phone;
        $transaction->email = $request->email;
        $transaction->address = $request->address;
        $transaction->address_two = $request->address_two;
        $transaction->proof_of_payment = $request->file('proof_of_payment')->store('transaction/proof_of_payment', ['disk' => 'my_files']);
        $transaction->save();

        $product_carts = Cookie::get('product_carts');

        $productCarts = ! empty($product_carts) ? json_decode($product_carts) : [];

        foreach($productCarts as $productCart) {
            $transactionItem = new TransactionItem();
            $transactionItem->transaction_id = $transaction->id;
            $transactionItem->product_id = $productCart->id;
            $transactionItem->quantity = $productCart->quantity;
            $transactionItem->total_price = $productCart->total_price;
            $transactionItem->save();
        }

        Cookie::queue(Cookie::forget('product_carts'));

        return redirect()->route('shop-page')->with('success_message', 'Successfully checkout!');
    }

}
