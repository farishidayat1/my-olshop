@extends('app.frontend.layout')

<style>
    .nav-item a {
        color: #E199A5;
    }

    .nav-item a.active {
        color: #495057 !important;
    }

    
</style>

@section('content')
<section style="background-attachment:fixed; background-image: url('/template-asset-frontend/assets/home-bg.jpg'); background-size:cover; background-position:center;">
    @if(session()->has('success_message'))
        <div class="alert alert-success">
            {{ session()->get('success_message') }}
        </div>
    @endif
    @if(session()->has('error_message'))
        <div class="alert alert-error">
            {{ session()->get('error_message') }}
        </div>
    @endif
    <div class="container shop-page">
        <div class="row">
            <div class="col-md-12">
                <div class="title">
                    <h1>
                        Our Product
                    </h1>
                </div>
                <hr style="background-color: #E199A5; height:2px; width:100px">
                {{-- <div class="description">
                    <p class="desc-normal">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Fugiat et eius inventore? Reiciendis itaque porro deleniti est cumque? Sunt fugit amet eius dolore nesciunt? Quasi non tempore voluptatum eveniet. Excepturi!</p>
                </div> --}}
            </div>
            <div class="col-md-12 title d-flex justify-content-center">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                      <a class="nav-link @if($tab == 'all') active @endif" href="{{ route('shop-page', ['tab' => 'all']) }}">All</a>
                    </li>
                    @foreach($productCategories as $productCategory)
                        <li class="nav-item">
                            <a class="nav-link @if($tab == $productCategory->name) active @endif" href="{{ route('shop-page', ['tab' => $productCategory->name]) }}">{{ $productCategory->name }}</a>
                        </li>
                    @endforeach
                  </ul>
            </div>
            @foreach($products as $product)
                <div class="col-sm-12 col-md-6 col-lg-4 my-5" data-toggle="modal" data-target="#productModal" onclick="setDataModal({{$product}})">
                    <div class="card border-0">
                        <div class="card-product text-center">
                            <img src="{{ $product->signed_image_url }}" alt="">
                            <div class="padding-custom">
                                <div class="name-product">
                                    <p>{{ $product->name }}</p>
                                </div>
                                <hr>
                                <div class="price">
                                    <h5>Rp. {{ $product->price_formatted }}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>   
            @endforeach
            @if(count($products) == 0)
                <div class="col-sm-12 col-md-6 col-lg-4 my-5">
                    <div class="card border-0">
                        <div class="card-product text-center">
                            {{-- <img src="{{ $product->signed_image_url }}" alt=""> --}}
                            <div class="padding-custom">
                                <div class="name-product">
                                    <p>Product not found</p>
                                </div>
                                <hr>
                                <div class="price">
                                    {{-- <h5>Rp. {{ $product->price_formatted }}</h5> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
                
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="productModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="card-product-modal d-flex">
              <img id="exampleModalImage">
              <div class="desc-modal">
                  {{-- <h6 class="pb-2">Lorem ipsum dolor sit amet consectetur adipisicing elit. Eligendi sit delectus sint, iste nam esse a! Veritatis ab cupiditate, optio necessitatibus sunt provident in officia consectetur laborum qui, praesentium accusantium.</h6>
                  <p>Brand: <span>Palm</span></p>
                  <p>Product Code: <span>Product 2</span> </p>
                  <p>Availability: <span>In Stock</span></p> --}}
              </div>
          </div>
        </div>
        <div class="modal-footer">
          <form action="{{ url('add-cart') }}" method="POST">
            @csrf
            <input type="hidden" name="product_id"/>
            <input type="number" id="quantity" name="quantity" min="1" max="5" style="width: 100px; height:40px" placeholder="qty">
            <button type="submit" class="btn btn-add">Add to cart</button>
          </form>
        </div>
      </div>
    </div>
</div>

<script type="text/javascript">
    function setDataModal(product) {
        $("#exampleModalLabel").text(product.name)
        $("#exampleModalImage").attr('src', product.signed_image_url);
        $("[name='product_id']").val(product.id);
    }
</script>
@endsection