@extends('app.frontend.layout')

@section('content')
<div class="contact-banner" style="background-image:url({{ asset('template-asset-frontend/assets/banner5.jpg') }}); background-size:cover; background-position:center; height:300px; background-attachment:fixed"></div>

<section id="cart">
    <div class="container">
        <div class="cart-page">
            <h1>Checkout Form</h1>
            {{-- <p class="text-center py-3">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Tempore voluptate quia reiciendis earum aspernatur, cupiditate non sed, optio sequi nihil a aperiam enim quis incidunt molestias id. Architecto, voluptatum ullam?</p> --}}
            <div class="row py-3">
                <div class="col-md-8">
                    <div class="title-form pb-3">
                        <h4>Form Pembayaran</h4>
                    </div>
                    <form action="{{ url('/checkout') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label>Nama lengkap</label>
                                <input type="text" class="form-control" required name="name" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>No Telepon</label>
                            <input type="text" name="phone" required class="form-control" placeholder="">
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" required name="email" placeholder="">
                        </div>
                        <div class="form-group">
                            <label>Alamat (provinsi, kota, kecamatan, kode pos)</label>
                            <input type="text" class="form-control" required name="address" placeholder="">
                        </div>
                        <div class="form-group">
                            <label>Alamat 2 (nama jalan, rt rw , no rumah)</label>
                            <input type="text" class="form-control" required name="address_two" placeholder="">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlFile1">Upload bukti pembayaran</label>
                            <input type="file" class="form-control-file pt-1" required name="proof_of_payment" id="exampleFormControlFile1">
                        </div>
                        <button type="submit" class="btn  btn-continue" style="width:100%">Continue to Checkout</button>
                    </form>
                    <hr>
                </div>
                <div class="col-md-4 card2-custom">
                    <div class="d-flex custom-position pb-3">
                        <h4>Your Cart</h4>
                        <p class="count-card">{{ $countProduct }}</p>
                    </div>
                    <div class="card">
                        @foreach($productCarts as $productCart) 
                            <div class="content-card pt-3">
                                <div class="d-flex descript-card">
                                    <p>{{ $productCart->name }}</p>
                                    <h6>{{ $productCart->total_price_formatted }}</h6>
                                </div>
                                <span>Qty : {{ $productCart->quantity }}</span>
                            </div>
                            <hr>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection