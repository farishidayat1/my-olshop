<div class="header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-expand-lg navbar-light" style="align-items: baseline;">
                    <img src="{{ asset('template-asset-frontend/assets/logo_second.png') }}" class="logo" alt="" style="width: 55px; margin: auto 0">
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('/') }}">Home</a>
                            </li>
                            {{-- <li class="nav-item">
                                <a class="nav-link" href="{{ url('/shop') }}">Shop</a>
                            </li> --}}
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('/contact') }}">Contact</a>
                            </li>
                            @php
                                $product_carts = Cookie::get('product_carts');
                                $productCarts = ! empty($product_carts) ? json_decode($product_carts) : [];
                                $totalCart = count($productCarts);
                            @endphp
                            <li class="nav-item" style="margin: auto 10px;  ">
                                <a href="{{ url('/cart') }}" style=" color:#dd959b; position:relative; font-size: 22px;">
                                    <i class="fa fa-shopping-cart">
                                        <p class="count-card">{{ $totalCart }}</p>
                                    </i>
                                </a>
                            </li>
                            {{-- <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    My Account
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="login.php">Login</a>
                                    <a class="dropdown-item" href="register.php">Register</a>
                                </div>
                            </li> --}}
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>