@extends('app.frontend.layout')

@section('content')
<div class="contact-banner" style="background-image:url('/template-asset-frontend/assets/banner4.jpg'); background-size:cover; background-position:center; height:300px; background-attachment:fixed"></div>

<section id="contact">
    <div class="container">
        <h1 class="text-center mb-3" style="font-weight: 600; font-family: 'beau'">Contact Us</h1>
        <hr style="background-color: #E199A5; height:2px; width:100px">
        {{-- <p class="text-center w-75 m-auto">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris interdum
            purus at sem ornare sodales. Morbi leo nulla, pharetra vel felis nec, ullamcorper condimentum quam.</p> --}}
        <div class="row">
            <div class="col-sm-12 col-md-6 col-lg-4 my-5">
                <div class="card border-0">
                    <div class="card-body text-center">
                        <i style="color: #E199A5;" class="fa fa-phone fa-5x mb-3" aria-hidden="true"></i>
                        <h4 class=" mb-3">Call Us</h4>
                        <p>+628-1234-5678</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-4 my-5">
                <div class="card border-0">
                    <div class="card-body text-center">
                        <i style="color: #E199A5;" class="fa fa-map-marker fa-5x mb-3" aria-hidden="true"></i>
                        <h4 class=" mb-3">Office Location</h4>
                        <address>Jl. Sultan Iskandar Muda, RT.10/RW.6, Kby. Lama Utara, Kec. Kby. Lama, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12240</address>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-4 my-5">
                <div class="card border-0">
                    <div class="card-body text-center">
                        <i style="color: #E199A5;" class="fa fa-globe fa-5x mb-3" aria-hidden="true"></i>
                        <h4 class=" mb-3">Email</h4>
                        <p>myolshopbusinnes@gmail.com</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15864.593186738737!2d106.783529!3d-6.2441792!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x6173704d396d5a99!2sGandaria%20City%20Mall!5e0!3m2!1sid!2sid!4v1649841852714!5m2!1sid!2sid" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade" class="mt-5"></iframe>

@endsection