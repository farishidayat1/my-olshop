<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('template-asset-frontend/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('template-asset-frontend/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('template-asset-frontend/css/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('template-asset-frontend/css/slick-theme.css') }}">
    <script src="{{ asset('template-asset/vendor/jquery/jquery.min.js') }}"></script>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
    <title>My Olshop</title>
  </head>
<body>

<style>
    .header {
        height: 80px;
        padding-top: 10px;
        /* position: fixed; */
        width: 100%;
        z-index: 10;
        background-color: #fff;
        box-shadow: 0px 3px 5px rgb(100 100 100 / 49%);
    }

    .navbar-light .navbar-nav .nav-link {
        font-weight: 600;
        margin: 0 15px;
        text-transform: uppercase;
        color: #000;
        font-size: 12px;
        letter-spacing: 1px;
    }

    .login {
        border: 1px solid #000;
        width: 100px;
        text-align: center;
        margin: 0 2px !important;
    }

    .register {
        border: 1px solid #000;
        width: 100px;
        text-align: center;
        margin: 0 2px !important;
    }
</style>

@include('app.frontend.partial.header') 

@yield('content')

@include('app.frontend.partial.footer')
