@extends('app.admin.layout')
<style>
    .btn-primary {
        margin-bottom: 20px;
    }
</style>
@section('content')  
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-flex justify-content-between">
            <h1 class="h3 mb-2 text-gray-800">Products</h1>
            <a class="btn btn-primary btn-icon-split" href="{{ route('products.create') }}">
                <span class="text">Create</span>
            </a>            
        </div>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            @if(session()->has('success_message'))
                <div class="alert alert-success">
                    {{ session()->get('success_message') }}
                </div>
            @endif
            @if(session()->has('error_message'))
                <div class="alert alert-error">
                    {{ session()->get('error_message') }}
                </div>
            @endif
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">List of Products</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Number</th>
                                <th>Name</th>
                                <th>Price</th>
                                <th>Category</th>
                                <th>Image</th>
                                <th>Created At</th>
                                <th>Options</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($products as $index => $product)
                                <tr>
                                    <td>{{ $index+1 }}</td>
                                    <td>{{ $product->name }}</td>
                                    <td>{{ $product->price_formatted }}</td>
                                    <td>{{ $product->category->name }}</td>
                                    <td>
                                        <img src="{{ $product->signed_image_url }}" style="width: 50px; height: 50px;"/>    
                                    </td>
                                    <td>
                                        {{ date('d M Y', strtotime($product->created_at)) }}
                                    </td>
                                    <td>
                                        <div class="d-flex">
                                            <div>
                                                <a class="btn btn-primary mr-2" href="{{ route('products.edit', [$product->id]) }}">
                                                    <i class="fas fa-pen"></i>
                                                </a>
                                            </div>
                                            <div>
                                                <form action="{{ route('products.delete' )}}" id="form-delete-{{ $product->id }}" method="POST" style="display: none;">
                                                    @csrf
                                                    <input type="hidden" name="id" value="{{ $product->id }}"/>
                                                </form>
                                                <a class="btn btn-danger" onclick="confirmationDelete({{ $product->id }})">
                                                    <i class="fas fa-trash"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                            @endforeach
                        </tbody>
                        
                    </table>
                </div>
            </div>
        </div>

    </div>

    <script>
        function confirmationDelete(id) {
            const alert = confirm('Are you sure to delete this product ?')
            if(alert) {
                $("#form-delete-"+id).submit();
            }
        }
    </script>
@endsection