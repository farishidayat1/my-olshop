@extends('app.admin.layout')

@section('content')  
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-flex justify-content-between">
            <h1 class="h3 mb-2 text-gray-800">Products</h1>
        </div>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            @if(count($errors) > 0)
				<div class="alert alert-danger">
					@foreach ($errors->all() as $error)
				    	{{ $error }} <br/>
					@endforeach
				</div>
            @endif
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Edit Products</h6>
            </div>
            <div class="card-body">
                <form action="{{ route('products.update') }}" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="id" value="{{ $product->id }}"/>
                    @csrf
                    <div class="mb-3">
                      <label for="name" class="form-label">Name</label>
                      <input type="text" class="form-control" name="name" id="name" value="{{ $product->name }}">
                    </div>
                    <div class="mb-3">
                        <label for="price" class="form-label">Price</label>
                        <input type="number" class="form-control" name="price" id="price" value="{{ $product->price }}">
                    </div>
                    <div class="mb-3">
                        <label for="category_id" class="form-label">Category</label>
                        <select class="form-control" name="category_id">
                            <option value="">Choose a category</option>
                            @foreach($product_categories as $category)
                                <option value="{{ $category->id }}" @if($category->id == $product->category_id ) selected @endif>{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="image" class="form-label">Image</label>
                        <input type="file" class="form-control" name="image" id="image">
                    </div>
                   
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>

    </div>
@endsection