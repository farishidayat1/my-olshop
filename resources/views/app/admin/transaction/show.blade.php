@extends('app.admin.layout')
<style>
    .btn-primary {
        margin-bottom: 20px;
    }
</style>
@section('content')  
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-flex justify-content-between">
            <h1 class="h3 mb-2 text-gray-800">Detail of Transaction</h1>
            {{-- <a class="btn btn-primary btn-icon-split" href="{{ route('products.create') }}">
                <span class="text">Create</span>
            </a>             --}}
        </div>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            @if(session()->has('success_message'))
                <div class="alert alert-success">
                    {{ session()->get('success_message') }}
                </div>
            @endif
            @if(session()->has('error_message'))
                <div class="alert alert-error">
                    {{ session()->get('error_message') }}
                </div>
            @endif
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">View Transaction</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <tbody>
                            <tr>
                                <th>Name</th>
                                <td>{{ $transaction->name }}</td>
                            </tr>
                            <tr>
                                <th>Phone</th>
                                <td>{{ $transaction->phone }}</td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td>{{ $transaction->email }}</td>
                            </tr>
                            <tr>
                                <th>Created At</th>
                                <td>
                                    {{ date('d M Y', strtotime($transaction->created_at)) }}
                                </td>
                            </tr>
                            <tr>
                                <th>Address</th>
                                <td>
                                    {{ $transaction->address }}
                                </td>
                            </tr>
                            <tr>
                                <th>Address Two</th>
                                <td>
                                    {{ $transaction->address_two }}
                                </td>
                            </tr>
                            <tr>
                                <th>Proof Of Payment</th>
                                <td>
                                    <img src="{{ $transaction->signed_proof_of_payment_url }}" style="width: 150px; height: auto;"/>    
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">List of Transaction Items</h6>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Number</th>
                                <th>Name</th>
                                <th>Image</th>
                                <th>Quantity</th>
                                <th>Total Price</th>
                                <th>Created At</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($transaction->items as $index => $item)
                                <tr>
                                    <td>{{ $index+1 }}</td>
                                    <td>{{ $item->product->name }}</td>
                                    <td>
                                        <img src="{{ $item->product->signed_image_url }}" style="width: 50px; height: 50px;"/>    
                                    </td>
                                    <td>{{ $item->quantity }}</td>
                                    <td>{{ $item->total_price_formatted }}</td>
                                    <td>
                                        {{ date('d M Y', strtotime($transaction->created_at)) }}
                                    </td>
                            @endforeach
                        </tbody>
                        
                    </table>
                </div>
            </div>
        </div>

    </div>
@endsection