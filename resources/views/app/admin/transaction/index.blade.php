@extends('app.admin.layout')
<style>
    .btn-primary {
        margin-bottom: 20px;
    }
</style>
@section('content')  
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-flex justify-content-between">
            <h1 class="h3 mb-2 text-gray-800">Transactions</h1>
            {{-- <a class="btn btn-primary btn-icon-split" href="{{ route('products.create') }}">
                <span class="text">Create</span>
            </a>             --}}
        </div>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            @if(session()->has('success_message'))
                <div class="alert alert-success">
                    {{ session()->get('success_message') }}
                </div>
            @endif
            @if(session()->has('error_message'))
                <div class="alert alert-error">
                    {{ session()->get('error_message') }}
                </div>
            @endif
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">List of Transactions</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Number</th>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Created At</th>
                                <th>Options</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($transactions as $index => $transaction)
                                <tr>
                                    <td>{{ $index+1 }}</td>
                                    <td>{{ $transaction->name }}</td>
                                    <td>{{ $transaction->phone }}</td>
                                    <td>{{ $transaction->email }}</td>
                                    <td>
                                        {{ date('d M Y', strtotime($transaction->created_at)) }}
                                    </td>
                                    <td>
                                        <a class="btn btn-primary mr-2" href="{{ route('transactions.show', [$transaction->id]) }}">
                                            <i class="fas fa-eye"></i>
                                        </a>
                                    </td>
                            @endforeach
                        </tbody>
                        
                    </table>
                </div>
            </div>
        </div>

    </div>
@endsection