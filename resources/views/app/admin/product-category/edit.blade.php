@extends('app.admin.layout')

@section('content')  
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-flex justify-content-between">
            <h1 class="h3 mb-2 text-gray-800">Products Category</h1>
        </div>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            @if(count($errors) > 0)
				<div class="alert alert-danger">
					@foreach ($errors->all() as $error)
				    	{{ $error }} <br/>
					@endforeach
				</div>
            @endif
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Edit Products Category</h6>
            </div>
            <div class="card-body">
                <form action="{{ route('products-category.store') }}" method="POST" enctype="multipart/form-data">
                    <input type="hidden" class="form-control" name="id" value="{{ $product_category->id }}">
                    @csrf
                    <div class="mb-3">
                      <label for="name" class="form-label">Name</label>
                      <input type="text" class="form-control" name="name" id="name" value="{{ $product_category->name }}">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>   
            </div>
        </div>

    </div>
@endsection