<?php

use App\Http\Controllers\Application\Admin\HomeController;
use App\Http\Controllers\Application\Admin\ProductCategoryController;
use App\Http\Controllers\Application\Admin\ProductController;
use App\Http\Controllers\Application\Admin\TransactionController;
use App\Http\Controllers\Application\Frontend\HomeController as FrontendHomeController;
use App\Http\Controllers\Auth\AuthController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [FrontendHomeController::class, 'shop'])->name('shop-page');
Route::post('/add-cart', [FrontendHomeController::class, 'addCart']);
Route::get('/cart', [FrontendHomeController::class, 'cart']);
Route::post('/checkout', [FrontendHomeController::class, 'checkout']);

Route::get('/contact', function () {
    return view('app.frontend.contact');
});

Route::prefix('admin')->group(function() {
    Route::get('/', [AuthController::class, 'index'])->name('login.form');
    Route::post('/login', [AuthController::class, 'login'])->name('login');
    Route::get('/register', [AuthController::class, 'registerForm'])->name('register');
    Route::post('/register', [AuthController::class, 'register'])->name('register');
    Route::get('/logout', [AuthController::class, 'logout'])->name('logout');
});

Route::prefix('application/admin')->middleware('auth')->group(function() {
    Route::get('/', [HomeController::class, 'index'])->name('application');

    Route::prefix('products')->group(function() {
        Route::get('/', [ProductController::class, 'index'])->name('products');
        Route::get('/create', [ProductController::class, 'create'])->name('products.create');
        Route::post('/store', [ProductController::class, 'store'])->name('products.store');
        Route::get('/edit/{id}', [ProductController::class, 'edit'])->name('products.edit');
        Route::post('/update', [ProductController::class, 'update'])->name('products.update');
        Route::post('/delete', [ProductController::class, 'destroy'])->name('products.delete');
    });

    Route::prefix('products-category')->group(function() {
        Route::get('/', [ProductCategoryController::class, 'index'])->name('products-category');
        Route::get('/create', [ProductCategoryController::class, 'create'])->name('products-category.create');
        Route::post('/store', [ProductCategoryController::class, 'store'])->name('products-category.store');
        Route::get('/edit/{id}', [ProductCategoryController::class, 'edit'])->name('products-category.edit');
        Route::post('/update', [ProductCategoryController::class, 'update'])->name('products-category.update');
        Route::post('/delete', [ProductCategoryController::class, 'destroy'])->name('products-category.delete');
    });

    Route::prefix('transactions')->group(function() {
        Route::get('/', [TransactionController::class, 'index'])->name('transactions');
        Route::get('/show/{id}', [TransactionController::class, 'show'])->name('transactions.show');
    });
});

// Route::get('/', function () {
//     return view('welcome');
// });
